﻿using UnityEngine;
using System.Collections;

public class SimpleTeam : MonoBehaviour {

	public SimpleTeam otherTeam;
	public bool isTeam1;

	//TODO move states to FSM
	public enum TeamState {
		WAIT,
		ATTACKING,
		DEFENDING,
		PREPARE_FOR_KICKOFF
	}


	private const string MSG_GO_HOME = "Msg_GoHome";
	private const string MSG_RECEIVE_BALL = "Msg_ReceiveBall";
	private const string MSG_LOST_BALL = "Msg_LostBall";
	private const string MSG_SUPPORT = "Msg_Support";
	private const string MSG_PASS_TO = "Msg_PassTo";

	private TeamState state;

	private GameObject goal;

	public GameObject Goal {
		get {
			return goal;
		}
	}

	private GameObject soccerBall;
	private GameObject controllingPlayer;
	private GameObject supportPlayer;
	private GameObject chasePlayer;
	private int playersAtHome;

	public int PlayersAtHome {
		get {
			return playersAtHome;
		}
	}

	private SimpleSpotCalculator simpleSpotCalculator;

	public SimpleSpotCalculator SimpleSpotCalculator {
		get {
			return simpleSpotCalculator;
		}
	}

	public bool isAttacking() {
		return (state == TeamState.ATTACKING);
	}

	void Awake () {
		state = TeamState.WAIT;
		playersAtHome = 0;
	}

	void Start () {
		soccerBall = GameObject.FindWithTag("Ball");
		simpleSpotCalculator = GameObject.FindWithTag("Pitch").GetComponent<SimpleSpotCalculator>();

		//goal = transform.FindChild(GameObject.FindGameObjectWithTag("Goal").transform.name).gameObject;
		goal = simpleSpotCalculator.GoalForTeam(!isTeam1);

		StartCoroutine("TeamUpdateCheck");
	}

	// Update is called once per frame
	void Update () {

		if(state == TeamState.WAIT && Input.GetKeyUp(KeyCode.Space)) {
			PrepareForKickoff();
		}
	
	}

	IEnumerator TeamUpdateCheck()
	{
		yield return new WaitForSeconds(Constants.TEAM_UPDATE_FREQUENCY);

		GameObject closestPlayerToBall = ClosestPlayerToObject(soccerBall.transform.position, null, false);

		switch(state) {
		case TeamState.PREPARE_FOR_KICKOFF:
			break;
		case TeamState.DEFENDING:
			//tell closest player to go to the ball
			if(closestPlayerToBall && closestPlayerToBall != chasePlayer ) {
				if(chasePlayer && chasePlayer.GetComponent<SimplePlayerField>().isChasing()) {
					chasePlayer.SendMessage(MSG_GO_HOME);
				}
				closestPlayerToBall.SendMessage(MSG_RECEIVE_BALL);
				chasePlayer = closestPlayerToBall;
			}
			break;
		case TeamState.ATTACKING:
			if(controllingPlayer != null) {
				if(controllingPlayer != closestPlayerToBall) {
					//the controlling player should always be the one closest to the ball
					controllingPlayer.SendMessage(MSG_GO_HOME);
					closestPlayerToBall.SendMessage(MSG_RECEIVE_BALL);
					controllingPlayer = closestPlayerToBall;
				}
				//delegate a supporting player
				FindSupportPlayer(controllingPlayer);
			}
			break;
		}

		if(controllingPlayer == null && supportPlayer != null) {
			//don't have the ball any more, tell support player to go home
			if(supportPlayer.GetComponent<SimplePlayerField>().isSupporting()) {
				supportPlayer.SendMessage(MSG_GO_HOME);
			}
			supportPlayer = null;
		}

		BoxCollider b = goal.GetComponent<BoxCollider>();
		if(b.bounds.Contains(soccerBall.transform.position)) {
			PrepareForKickoff();
			otherTeam.PrepareForKickoff();
		}

		//check again later
		StartCoroutine("TeamUpdateCheck");
	}

	public bool IsShootOpen (Vector3 position) {
		return simpleSpotCalculator.IsShootOpen(position, isTeam1);
	}

	public bool IsThreatened (Vector3 position) {
		return simpleSpotCalculator.IsThreatened(position, isTeam1);
	}

	public GameObject FindPassPlayer (GameObject passerPlayer) {
		return simpleSpotCalculator.BestPassPlayer(passerPlayer, isTeam1);
	}

	public void FindSupportPlayer (GameObject currentPlayer) {
		Vector3 supportPosition = simpleSpotCalculator.BestSupportPosition(isTeam1);
		GameObject newSupportPlayer = ClosestPlayerToObject(supportPosition, currentPlayer, false);
		if(newSupportPlayer && newSupportPlayer != supportPlayer) {
			if(supportPlayer != null && supportPlayer.GetComponent<SimplePlayerField>().isSupporting()) {
				supportPlayer.SendMessage(MSG_GO_HOME);
			}
			newSupportPlayer.SendMessage(MSG_SUPPORT, supportPosition);
			supportPlayer = newSupportPlayer;
		}
	}

	/**
	 * Closest player on the team to the ball
	 * */
	public GameObject ClosestPlayerToObject (Vector3 objectPosition, GameObject currentPlayer, bool isPass) {
		GameObject playerClosestToBall = null;
		float closestDistance = -1;
		GameObject[] players = TeamPlayers();
		foreach(GameObject player in players) {
			float distance = (player.transform.position - objectPosition).magnitude;
			bool isSamePlayer = currentPlayer && (player == currentPlayer);
			bool isPassOk = !isPass || simpleSpotCalculator.IsPassOpen(player.transform.position, isTeam1);
			if(!isSamePlayer && (closestDistance < 0 || distance < closestDistance) && isPassOk) {
				closestDistance = distance;
				playerClosestToBall = player;
			}
		}
		return playerClosestToBall;
	}

	public void PrepareForKickoff() {
		//state = TeamState.PREPARE_FOR_KICKOFF;
		//tell all players to go back to their home positions
		playersAtHome = 0;
		BroadcastMessage(MSG_GO_HOME);
		state = TeamState.WAIT;
		soccerBall.GetComponent<SimpleSoccerBall>().GoToStart();
	}

	/**
	 * Returns all the players belonging to this team
	 * **/
	private GameObject[] TeamPlayers () {

		GameObject[] returnPlayers = new GameObject[Constants.NUM_FIELD_PLAYERS];
		int count = 0;
		for(int i = 0; i < transform.childCount; i++) {
			GameObject cur = transform.GetChild(i).gameObject;
			if((cur.tag == "Team1_Player" || cur.tag == "Team2_Player") && count < Constants.NUM_FIELD_PLAYERS) {
				returnPlayers[count] = cur;
				count++;
			}
		}

		return returnPlayers;


	}

	//***messages from players***
	void Msg_ArrivedToHome () {
		playersAtHome++;
		if(playersAtHome == Constants.NUM_FIELD_PLAYERS && otherTeam.PlayersAtHome == Constants.NUM_FIELD_PLAYERS) {
			//ready to start game
			Debug.Log("Players in position!");
			state = TeamState.DEFENDING;
		}

	}

	void Msg_ArrivedToBall (GameObject player) {
		//tell the other team we are attacking now
		otherTeam.SendMessage(MSG_LOST_BALL);
		controllingPlayer = player; //.GetComponent<SimplePlayerField>();
		chasePlayer = null;
		state = TeamState.ATTACKING;
	}

	void Msg_PassToMe (GameObject player) {
		Debug.Log("player " + player.ToString() + " requesting a pass!");
		//make sure the player is open for a pass
		if(controllingPlayer && simpleSpotCalculator.IsPassOpen(player.transform.position, isTeam1)) {
			controllingPlayer.SendMessage(MSG_PASS_TO, player);
		}

	}

	void Msg_Shoot (GameObject player) {
		state = TeamState.DEFENDING;
		controllingPlayer = null;

	}

	//***messages from other team***
	public void Msg_LostBall() {
		//Debug.Log(gameObject.ToString() + " lost control of the ball");
		state = TeamState.DEFENDING;
		controllingPlayer = null;

	}



}
