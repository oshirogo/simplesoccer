﻿using UnityEngine;
using System.Collections;

public class SimplePlayerBase : MonoBehaviour {

	public Transform headTransform;

	protected GameObject team;
	protected SimpleTeam simpleTeam;
	protected GameObject soccerBall;
	protected SimpleSoccerBall simpleSoccerBall;
	protected Quaternion initialHeadRotation;	


	void LateUpdate() {
		
		// turn head if necesary
		Vector3 relativePos = transform.InverseTransformPoint( soccerBall.transform.position );
		
		if ( relativePos.z > 0.0f ) {
			
			Quaternion lookRotation = Quaternion.LookRotation (soccerBall.transform.position + new Vector3(0, 1.0f,0) - headTransform.position);
			headTransform.rotation = lookRotation * initialHeadRotation;			
			headTransform.eulerAngles = new Vector3( headTransform.eulerAngles.x, headTransform.eulerAngles.y,  -90.0f);
			
		}
		
	}	

	protected void init () {
		soccerBall = GameObject.FindWithTag("Ball");
		team = transform.parent.gameObject;
		simpleTeam = team.GetComponent<SimpleTeam>();
		simpleSoccerBall = soccerBall.GetComponent<SimpleSoccerBall>();
		initialHeadRotation = transform.rotation * headTransform.rotation;

		animation["jump_backwards_bucle"].speed = 1.5f;
		animation["starting"].speed = 1.0f;
		animation["running"].speed = 1.2f;
		animation["pass"].speed = 1.8f;
		animation["rest"].speed = 1.0f;
		animation["turn"].speed = 1.3f;
		animation["fight"].speed = 1.2f;
	}



}
