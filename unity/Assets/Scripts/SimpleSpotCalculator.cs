﻿using UnityEngine;
using System.Collections;

public class SimpleSpotCalculator : MonoBehaviour {

	private const int numGridCols = 15;
	private const int numGridRows = 22;
	private Vector3 gridSize = new Vector3(Constants.GRID_SIZE, 1, Constants.GRID_SIZE);

	private const float PASS_INFLUENCE = 1;
	private const float GOAL_INFLUENCE = 0.5f;
	private const float PLAYER_INFLUENCE = 30;
	private const float SHOOT_DISTANCE = 30;
	private const float THREATENED_DISTANCE_BEHIND = 1.5f;
	private const float THREATENED_DISTANCE_AHEAD = 3;

	private const string BALL = "Ball";
	private const string GOAL_1 = "Goal1";
	private const string GOAL_2 = "Goal2";
	private const string TEAM_1_PLAYER = "Team1_Player";
	private const string TEAM_2_PLAYER = "Team2_Player";
	private const string TEAM_1_GOAL_KEEPER = "Team1_GoalKeeper";
	private const string TEAM_2_GOAL_KEEPER = "Team2_GoalKeeper";

	private enum DebugState {
		HIDE,
		GOAL_1,
		GOAL_2,
		BALL,
		TEAM_1,
		TEAM_2,
		TEAM_1_SUP,
		TEAM_2_SUP,
		TEAM_1_DRIBBLE,
		TEAM_2_DRIBBLE
	}

	private DebugState state = DebugState.HIDE;

	void OnGUI() {
		int space = 35;
		if (GUI.Button(new Rect(10, 10 + space * 0, 60, 30), "Hide")) {
			state = DebugState.HIDE;
		} else if (GUI.Button(new Rect(10, 10 + space * 1, 60, 30), "Goal 1")) {
			state = DebugState.GOAL_1;
		} else if (GUI.Button(new Rect(10, 10 + space * 2, 60, 30), "Goal 2")) {
			state = DebugState.GOAL_2;
		} else if (GUI.Button(new Rect(10, 10 + space * 3, 60, 30), "Ball")) {
			state = DebugState.BALL;
		} else if (GUI.Button(new Rect(10, 10 + space * 4, 60, 30), "Team 1")) {
			state = DebugState.TEAM_1;
		} else if (GUI.Button(new Rect(10, 10 + space * 5, 60, 30), "Team 2")) {
			state = DebugState.TEAM_2;
		} else if (GUI.Button(new Rect(10, 10 + space * 6, 60, 30), "Team 1 Sup")) {
			state = DebugState.TEAM_1_SUP;
		} else if (GUI.Button(new Rect(10, 10 + space * 7, 60, 30), "Team 2 Sup")) {
			state = DebugState.TEAM_2_SUP;
		} else if (GUI.Button(new Rect(10, 10 + space * 8, 60, 30), "Team 1 Dribble")) {
			state = DebugState.TEAM_1_DRIBBLE;
		} else if (GUI.Button(new Rect(10, 10 + space * 9, 60, 30), "Team 2 Dribble")) {
			state = DebugState.TEAM_2_DRIBBLE;
		}
		
	}

	void OnDrawGizmos() {
		switch(state) {
		case DebugState.HIDE:
			//don't show anything
			break;
		case DebugState.GOAL_1:
			ShowDistanceFromObjectWithTag(GOAL_1);
			break;
		case DebugState.GOAL_2:
			ShowDistanceFromObjectWithTag(GOAL_2);
			break;
		case DebugState.BALL:
			ShowDistanceFromObjectWithTag(BALL);
			break;
		case DebugState.TEAM_1:
			ShowDistanceFromObjectsWithTag(TEAM_1_PLAYER);
			break;
		case DebugState.TEAM_2:
			ShowDistanceFromObjectsWithTag(TEAM_2_PLAYER);
			break;
		case DebugState.TEAM_1_SUP:
			ShowSupport(true);
			break;
		case DebugState.TEAM_2_SUP:
			ShowSupport(false);
			break;
		case DebugState.TEAM_1_DRIBBLE:
			ShowDribble(true);
			break;
		case DebugState.TEAM_2_DRIBBLE:
			ShowDribble(false);
			break;
		}

	}

	private int TeamLayerMask(bool isTeam1) {
		return isTeam1 ? 1 << 9 : 1 << 8;
	}

	/**
	 * Returns true if the location is between some other 2 positions
	 * 
	 */
	private bool IsBetween2Positions(Vector3 location, Vector3 position1, Vector3 position2) {
		return !((position1.z > position2.z && position1.z > location.z) || (position1.z < position2.z && position1.z < location.z));
	}

	private Vector3[] AllPlayerPositions(bool isTeam1) {
		//int teamLayerMask = TeamLayerMask(!isTeam1);
		//Vector3 goalPosition = GoalForTeam(!isTeam1).transform.position;
		string playerTag = isTeam1 ? TEAM_1_PLAYER : TEAM_2_PLAYER;
		string goalKeeperTag = isTeam1 ? TEAM_1_GOAL_KEEPER : TEAM_2_GOAL_KEEPER;
		GameObject[] playerObjects = GameObject.FindGameObjectsWithTag(playerTag);
		GameObject[] goalKeeperObjects = GameObject.FindGameObjectsWithTag(goalKeeperTag);

		Vector3[] allPositions = new Vector3[playerObjects.Length + goalKeeperObjects.Length];
		for(int i=0; i<playerObjects.Length; i++) {
			allPositions[i] = playerObjects[i].transform.position;
		}
		for(int i=0; i<goalKeeperObjects.Length; i++) {
			allPositions[i + playerObjects.Length] = goalKeeperObjects[i].transform.position;
		}
		return allPositions;
	}

	/**
	 * Is Pass Open means there is a raycast between the ball and the location that doesn't cross paths with opponent team player
	 * 
	 */
	public bool IsPassOpen(Vector3 location, bool isTeam1) {
		//test raycast collisions using the other team's layer as a layer mask
		int teamLayerMask = TeamLayerMask(isTeam1);
		Vector3 ballPosition = GameObject.FindGameObjectWithTag(BALL).transform.position;
		Vector3 ballDistance = ballPosition - location;
		if(Physics.Raycast(new Vector3(location.x, 1, location.z), ballDistance, ballDistance.magnitude, teamLayerMask)) {
			//opponent between ball and this player
			return false;
		}
		if(IsThreatened(location, isTeam1)) {
			//opponent is too close to this player
			return false;
		}
		return true;
	}

	public bool IsShootOpen(Vector3 position, bool isTeam1) {
		//check opponents goal
		int teamLayerMask = TeamLayerMask(isTeam1);
		Vector3 location = GoalForTeam(!isTeam1).transform.position;
		Vector3 distance = position - location;

		if(distance.magnitude > SHOOT_DISTANCE) {
			//too far from goal
			return false;
		}

		//return true;
		bool isBlock = Physics.Raycast(new Vector3(location.x, 1, location.z), distance, distance.magnitude, teamLayerMask);
		return !isBlock;
	}

	/**
	 * 
	 * Threatened means there is an opponent within the "comfort zone" of the location, between the location and the goal
	 * 
	 */
	public bool IsThreatened(Vector3 playerPosition, bool isTeam1, float distanceBehind=THREATENED_DISTANCE_BEHIND, float distanceAhead=THREATENED_DISTANCE_AHEAD) {
		Vector3 goalPosition = GoalForTeam(isTeam1).transform.position;
		Vector3[] allPositions = AllPlayerPositions(!isTeam1);
		foreach(Vector3 opponentPosition in allPositions) {
			float distance = (opponentPosition - playerPosition).magnitude;
			//if opponent is coming from behind, use a choser threatened distance
			if(distance <= distanceBehind || (distance <= distanceAhead && IsBetween2Positions(opponentPosition, playerPosition, goalPosition))) {
				return true;
			}
		}
		return false;
	}

	public GameObject GoalForTeam(bool isTeam1) {
		string myGoal = isTeam1 ? GOAL_2 : GOAL_1; //use my own goal to calcluate score distance to other goal
		return GameObject.FindGameObjectWithTag(myGoal);
	}

	/**
	 * Return the player that has a clear pass who is closest to the goal
	*/ 
	public GameObject BestPassPlayer(GameObject passerPlayer, bool isTeam1) {
		Vector3 goalPosition = GoalForTeam(!isTeam1).transform.position;
		string team = isTeam1 ? TEAM_1_PLAYER : TEAM_2_PLAYER;
		GameObject[] playerObjects = GameObject.FindGameObjectsWithTag(team);
		GameObject bestPassPlayer = null;
		float closestDistanceToGoal = -1;
		for(var i=0; i<playerObjects.Length; i++) {
			Vector3 position = playerObjects[i].transform.position;
			if(playerObjects[i] != passerPlayer && IsPassOpen(position, isTeam1)) {
				float distance = (position - goalPosition).magnitude;
				if(closestDistanceToGoal < 0 || distance < closestDistanceToGoal) {
					bestPassPlayer = playerObjects[i];
					closestDistanceToGoal = distance;
				}
			}
		}
		return bestPassPlayer;
	}

	/**
	 * Checks everywhere on the pitch to find the best place for a player to be, combining opponent positions and proximity to the goal
	*/ 
	private Vector3 BestPosition(bool isTeam1, bool isDebug, int startX, int endX, int startZ, int endZ) {
		Vector3 goalPosition = GoalForTeam(!isTeam1).transform.position;

		if(isDebug) {
			Gizmos.color = Color.black;
			Gizmos.DrawCube(goalPosition, gridSize);

		}

		float highScore = -1000; //TODO better initial value for high score
		Vector3 highScoreLocation = new Vector3(0, 0, 0);
		
		for(int x = startX; x <= endX; x++) {
			for(int z = startZ; z <= endZ; z++) {
				float xPos = (x - (numGridCols / 2)) * gridSize.x - 1;
				float zPos = (z - (numGridRows / 2)) * gridSize.z + 2;
				Vector3 location = new Vector3(xPos, 0, zPos);
				float score = 100; //doesn't have to be 100, but good for debugging to show colors

				//the closer to the goal the better
				Vector3 goalDistance = location - goalPosition;
				score -= goalDistance.magnitude * GOAL_INFLUENCE;

				//closer to opponents is bad
				if(IsThreatened(location, isTeam1, gridSize.x * 2, gridSize.x * 2)) {
					score -= PLAYER_INFLUENCE;
				}
				
				if(score > highScore) {
					highScore = score;
					highScoreLocation = location;
					
				}

				if(isDebug) {
					Gizmos.color = ColorFromDistance(100 - score);
					Gizmos.DrawCube(location, gridSize);
				}
				
			}
			
		}

		if(isDebug) {
			Gizmos.color = Color.magenta;
			Gizmos.DrawCube(highScoreLocation, gridSize);
		}

		return highScoreLocation;

	}

	
	public Vector3 BestSupportPosition(bool isTeam1, bool isDebug=false) {
		Vector3 bestPosition = BestPosition(isTeam1, isDebug, 0, numGridCols, 0, numGridRows);

		Vector3 ballPosition = GameObject.FindGameObjectWithTag(BALL).transform.position;

		//don't get too close to ball
		float sbX = ballPosition.x;
		float bpX = bestPosition.x;
		float x;
		if(bpX < sbX) {
			x = Mathf.Clamp(bpX, bpX, sbX - Constants.GRID_SIZE * Constants.SUPPORT_BALL_SEPERATE_DISTNACE);
		} else {
			x = Mathf.Clamp(bpX, sbX + Constants.GRID_SIZE * Constants.SUPPORT_BALL_SEPERATE_DISTNACE, bpX);
		}
		return new Vector3(x, bestPosition.y, bestPosition.z);
	}

	/**
	 * Uses the same formula as finding support positions, exept it only checks the regions directly in front of/next to the ball
	*/
	public Vector3 BestDribblePosition(bool isTeam1, bool isDebug=false) {
		Vector3 offset = new Vector3((gridSize.x * numGridCols / 2) + 1, 1, (gridSize.z * numGridRows / 2) + 0.2f);
		Vector3 ballPosition = GameObject.FindGameObjectWithTag(BALL).transform.position + offset;
		int goalZ = (int)Mathf.Clamp (GoalForTeam(!isTeam1).transform.position.z, -2, 2);
		int ballX = (int)Mathf.Floor(ballPosition.x / gridSize.x);
		int ballZ = (int)Mathf.Floor(ballPosition.z / gridSize.z);
		int startX = (int)Mathf.Clamp(ballX - 2, 0, numGridCols);
		int endX = (int)Mathf.Clamp(ballX + 2, 0, numGridCols);
		int startZ = (int)Mathf.Clamp(ballZ, 0, numGridRows);
		int endZ = (int)Mathf.Clamp(ballZ + goalZ, 0, numGridRows);
		if(endZ < startZ) {
			int tmp = startZ;
			startZ = endZ;
			endZ = tmp;
		}


		return BestPosition(isTeam1, isDebug, startX, endX, startZ, endZ);
	}

	private void ShowSupport(bool isTeam1) {
		Gizmos.color = Color.blue;
		Gizmos.DrawCube(BestSupportPosition(isTeam1, true), gridSize);
		
	}

	private void ShowDribble(bool isTeam1) {
		BestDribblePosition(isTeam1, true);
	}

	private void ShowDistanceFromObjectsWithTag(string tag) {
		GameObject[] allObjects = GameObject.FindGameObjectsWithTag(tag);
		Vector3[] allPositions = new Vector3[allObjects.Length];
		for(var i=0; i<allObjects.Length; i++) {
			allPositions[i] = allObjects[i].transform.position;
		}

		for(int x = 0; x < numGridCols; x++) {
			for(int z = 0; z < numGridRows; z++) {
				float xPos = (x - (numGridCols / 2)) * gridSize.x - 1;
				float zPos = (z - (numGridRows / 2)) * gridSize.z + 2;
				Vector3 location = new Vector3(xPos, 0, zPos);
				float magnitude = 0;
				foreach(Vector3 position in allPositions) {
					Vector3 distance = location - position;
					magnitude += distance.magnitude;
				}
				magnitude = magnitude / allPositions.Length;
				Gizmos.color = ColorFromDistance(magnitude);
				Gizmos.DrawCube(location, gridSize);
			}
		}

	}

	private void ShowDistanceFromObjectWithTag(string tag) {
		Vector3 position = GameObject.FindGameObjectWithTag(tag).transform.position;
		
		for(int x = 0; x < numGridCols; x++) {
			for(int z = 0; z < numGridRows; z++) {
				float xPos = (x - (numGridCols / 2)) * gridSize.x - 1;
				float zPos = (z - (numGridRows / 2)) * gridSize.z + 2;
				Vector3 location = new Vector3(xPos, 0, zPos);
				Vector3 distance = location - position;
				Gizmos.color = ColorFromDistance(distance.magnitude);
				Gizmos.DrawCube(location, gridSize);
			}
		}
	}

	private Color ColorFromDistance(float distance) {
		return Color.Lerp(Color.red, Color.white, distance / 100);
	}
}
