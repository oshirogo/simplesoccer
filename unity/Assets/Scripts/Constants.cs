﻿public class Constants {
	//PITCH
	public static float GRID_SIZE = 5;
	public static float SUPPORT_BALL_SEPERATE_DISTNACE = 3; //support position must be at least 2 grids above or below ball

	//TEAM
	public static int NUM_FIELD_PLAYERS = 4;
	public static float TEAM_UPDATE_FREQUENCY = 0.5f; //number of seconds between checks for new supporter, etc

	//PLAYER
	public static float SPEED_RUN = 5.5f;
	public static float SPEED_DRIBBLE = 3.5f;
	public static float ARRIVE_DISTANCE = 0.7f; //if player is this close to destination, he can say he arrived
	public static float NEXT_MOVE_FREQUENCY = 0.35f; //number of seconds between checks for new move
	public static float MIN_DRIBBLE_TIME = 1; //number of seconds between after receiving the ball before a pass can be made
	public static float KICK_ACCURACY = 0.8f; //between 0 and 1, lower means worse accuracy when kicking

}
