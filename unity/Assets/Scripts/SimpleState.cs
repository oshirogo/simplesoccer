﻿using UnityEngine;
using System.Collections;

namespace SimpleState
{
	public interface SimpleState {
		
		//this will execute when the state is entered
		void Enter(SimplePlayerField simplePlayer);
		
		//this is the states normal update function
		void Execute(SimplePlayerField simplePlayer);

		//this is not called every update cycle, only once in a while
		void NextMoveCheck(SimplePlayerField simplePlayer);
		
		//this will execute when the state is exited. 
		void Exit(SimplePlayerField simplePlayer);
	}

	public class START : SingletonBase<START>, SimpleState {
		public void Enter(SimplePlayerField simplePlayer) {}
		public void Execute(SimplePlayerField simplePlayer) {}
		public void NextMoveCheck(SimplePlayerField simplePlayer) {}
		public void Exit(SimplePlayerField simplePlayer) {}
	}

	public class WAIT : SingletonBase<WAIT>, SimpleState {
		public void Enter(SimplePlayerField simplePlayer) {
			simplePlayer.StateColor = Color.black;
		}

		public void Execute(SimplePlayerField simplePlayer) {
			//move to home position if not there yet
			if(simplePlayer.AtHomePosition()) {
				simplePlayer.animation.Play("rest");
			} else {
				simplePlayer.GoTo(simplePlayer.HomePosition(), Constants.SPEED_RUN, "running");
			}
		}

		public void NextMoveCheck(SimplePlayerField simplePlayer) {
			simplePlayer.RequestPassIfOpen();
		}
		
		public void Exit(SimplePlayerField simplePlayer) {}
	}

	public class DRIBBLE : SingletonBase<DRIBBLE>, SimpleState {
		public void Enter(SimplePlayerField simplePlayer) {
			simplePlayer.StateColor = Color.yellow;
			simplePlayer.ControlBall();
		}

		public void Execute(SimplePlayerField simplePlayer) {
			float distance = simplePlayer.GoTo(simplePlayer.DribbleRegion, Constants.SPEED_DRIBBLE, "running_ball");
			if(distance < Constants.ARRIVE_DISTANCE) {
				simplePlayer.ArriveToDribble();
			}
		}

		public void NextMoveCheck(SimplePlayerField simplePlayer) {
			//shoot if possible, otherwise pass or continue dribbling
			simplePlayer.ShootPassOrContinueDribble();
		}

		public void Exit(SimplePlayerField simplePlayer) {}
	}

	public class CHASE_BALL : SingletonBase<CHASE_BALL>, SimpleState {
		public void Enter(SimplePlayerField simplePlayer) {
			simplePlayer.StateColor = Color.cyan;
		}

		public void Execute(SimplePlayerField simplePlayer) {
			float distance = simplePlayer.GoTo(simplePlayer.ChaseBallPosition(), Constants.SPEED_RUN, "running");
			if(distance < Constants.ARRIVE_DISTANCE) {
				simplePlayer.ArriveToBall();
			}
		}

		public void NextMoveCheck(SimplePlayerField simplePlayer) {}

		public void Exit(SimplePlayerField simplePlayer) {}
	}

	public class RETURN_TO_HOME_REGION : SingletonBase<RETURN_TO_HOME_REGION>, SimpleState {
		public void Enter(SimplePlayerField simplePlayer) {
			simplePlayer.StateColor = Color.gray;
		}

		public void Execute(SimplePlayerField simplePlayer) {
			float distance = simplePlayer.GoTo(simplePlayer.DefendHomeRegion, Constants.SPEED_RUN, "running");
			if(distance < Constants.ARRIVE_DISTANCE) {
				simplePlayer.ArriveToHome();
			}
		}

		public void NextMoveCheck(SimplePlayerField simplePlayer) {
			simplePlayer.RequestPassIfOpen();
		}

		public void Exit(SimplePlayerField simplePlayer) {}
	}

	public class SUPPORT_ATTACKER : SingletonBase<SUPPORT_ATTACKER>, SimpleState {
		public void Enter(SimplePlayerField simplePlayer) {
			simplePlayer.StateColor = Color.green;
		}

		public void Execute(SimplePlayerField simplePlayer) {
			float distance = simplePlayer.GoTo(simplePlayer.SupportRegion, Constants.SPEED_RUN, "running");
			if(distance < Constants.ARRIVE_DISTANCE) {
				simplePlayer.ArriveToSupport();
			}
		}

		public void NextMoveCheck(SimplePlayerField simplePlayer) {
			simplePlayer.RequestPassIfOpen();
		}

		public void Exit(SimplePlayerField simplePlayer) {}
	}
}
