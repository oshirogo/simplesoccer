﻿using UnityEngine;
using System.Collections;

public class FSM {

	private SimpleState.SimpleState state;
	private SimplePlayerField simplePlayer;

	public SimpleState.SimpleState State {
		get {
			return state;
		}
	}

	public FSM(SimplePlayerField simplePlayer) {
		this.state = SimpleState.START.Instance;
		this.simplePlayer = simplePlayer;
		this.state.Enter(simplePlayer);
	}

	public void Update() {
		state.Execute(simplePlayer);
	}

	public void NextMoveCheck() {
		state.NextMoveCheck(simplePlayer);
	}

	public void ChangeState(SimpleState.SimpleState newState) {
		state.Exit(simplePlayer);
		state = newState;
		state.Enter(simplePlayer);
	}
}
