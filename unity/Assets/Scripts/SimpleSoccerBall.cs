﻿using UnityEngine;
using System.Collections;

public class SimpleSoccerBall : MonoBehaviour {

	public GameObject owner; // the player with the ball
	public Transform shadowBall;

	private const float SPIN_VELOCITY = 0.3f;

	private Vector3 initialPosition;

	void Start() {
		initialPosition = transform.position;
	}

	void LateUpdate() {
		
		shadowBall.position = new Vector3( transform.position.x, 0.35f ,transform.position.z );
		shadowBall.rotation = Quaternion.identity;
		
	}

	void Update () {
		if ( owner ) {
			//pot ball in front of the owner
			transform.position = owner.transform.position + owner.transform.forward/1.5f + owner.transform.up/5.0f; 
			transform.position = new Vector3(transform.position.x, initialPosition.y, transform.position.z);

			//give the ball spin
			transform.RotateAround( owner.transform.right, owner.transform.forward.magnitude * SPIN_VELOCITY );


		}
	}

	public void GoTo(Vector3 position) {
		owner = null; //ball in movement means nobody controls it
		Vector3 directionBall = (position - transform.position).normalized;
		float distanceBall = (position - transform.position).magnitude * 1.4f;
		distanceBall = Mathf.Clamp( distanceBall, 15.0f, 40.0f );
		gameObject.GetComponent<Rigidbody>().velocity = new Vector3(directionBall.x * distanceBall, distanceBall/5.0f, directionBall.z * distanceBall );
	}

	public void GoToStart() {
		transform.position = initialPosition;
		gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
	}
}
