﻿using UnityEngine;
using System.Collections;

public class SimplePlayerField : SimplePlayerBase {

	public GameObject attackPosition;
	public GameObject defendPosition;
	public Color StateColor;
	
	
	private const string MSG_ARRIVED_TO_BALL = "Msg_ArrivedToBall";
	private const string MSG_ARRIVED_TO_HOME = "Msg_ArrivedToHome";
	private const string MSG_PASS_TO_ME = "Msg_PassToMe";
	private const string MSG_SHOOT = "Msg_Shoot";
	
	

	
	private Vector3 defendHomeRegion;
	public Vector3 DefendHomeRegion {
		get {
			return defendHomeRegion;
		}
	}
	
	private Vector3 dribbleRegion;
	public Vector3 DribbleRegion {
		get {
			return dribbleRegion;
		}
	}
	
	private Vector3 supportRegion;
	public Vector3 SupportRegion {
		get {
			return supportRegion;
		}
	}
	private Vector3 attackHomeRegion;
	private FSM fsm;
	private float arriveSpotTime;
	
	void Awake () {
		fsm = new FSM(this);
		ChangeState(SimpleState.WAIT.Instance);
		animation.Stop();
		arriveSpotTime = 0;
	}

	// Use this for initialization
	void Start () {
		init ();

		// set animations speed to fit perfect movements
		animation["starting_ball"].speed = 1.0f;
		animation["running_ball"].speed = 1.0f;
		animation["tackle"].speed = 2.0f;
		
		defendHomeRegion = defendPosition.transform.position;
		attackHomeRegion = attackPosition.transform.position;
		dribbleRegion = new Vector3(0, 0, 0);
		
		
		StartCoroutine("NextMoveCheck");
		
	}
	
	void Update () {
		fsm.Update();
	}
	
	IEnumerator NextMoveCheck()
	{
		yield return new WaitForSeconds(Constants.NEXT_MOVE_FREQUENCY);
		
		fsm.NextMoveCheck();
		
		//check again later
		StartCoroutine("NextMoveCheck");
	}
	
	void OnDrawGizmos() {
		if(simpleTeam) {
			Color teamColor = simpleTeam.isTeam1 ? Color.blue : Color.red;
			Gizmos.color = teamColor;
			Gizmos.DrawWireSphere(transform.position, 3);
		}
		Gizmos.color = StateColor; 
		if(fsm == null) {
			return;
		}
		if(fsm.State == SimpleState.DRIBBLE.Instance) {
			Gizmos.DrawCube(dribbleRegion, new Vector3(5, 1, 5));
			Vector3 goalPosition = simpleTeam.SimpleSpotCalculator.GoalForTeam(!simpleTeam.isTeam1).transform.position;
			Gizmos.DrawLine(transform.position + (Vector3.up * 2), goalPosition);
		} else if(fsm.State == SimpleState.SUPPORT_ATTACKER.Instance) {
			Gizmos.DrawCube(supportRegion, new Vector3(5, 1, 5));
		}
		Gizmos.DrawWireSphere(transform.position, 10);
	}
	
	public void ReceivePass(Vector3 ballDestination) {
		ChangeState(SimpleState.CHASE_BALL.Instance);
	}
	
	public bool isSupporting() {
		return (fsm.State == SimpleState.SUPPORT_ATTACKER.Instance);
	}
	
	public bool isChasing() {
		return (fsm.State == SimpleState.CHASE_BALL.Instance);
	}
	
	public void ChangeState(SimpleState.SimpleState state) {
		fsm.ChangeState(state);
	}
	
	public Vector3 ChaseBallPosition() {
		return new Vector3(soccerBall.transform.position.x, transform.position.y, soccerBall.transform.position.z);
	}
	
	public void ControlBall() {
		simpleSoccerBall.owner = gameObject;
	}
	
	public void ArriveToDribble() {
		dribbleRegion = simpleTeam.SimpleSpotCalculator.BestDribblePosition(simpleTeam.isTeam1);
		//at where we should be
		animation.Play("rest");
	}
	
	public void ArriveToBall() {
		arriveSpotTime = Time.time;
		team.SendMessage(MSG_ARRIVED_TO_BALL, transform.gameObject);
		ChangeState(SimpleState.DRIBBLE.Instance);
	}
	
	public void ArriveToSupport() {
		if(!simpleTeam.IsThreatened(transform.position)) {
			team.SendMessage(MSG_PASS_TO_ME, transform.gameObject);
		}
		ChangeState(SimpleState.WAIT.Instance);
	}
	
	public void ArriveToHome() {
		team.SendMessage(MSG_ARRIVED_TO_HOME, transform.gameObject);
		ChangeState(SimpleState.WAIT.Instance);
	}
	
	public void RequestPassIfOpen() {
		if(simpleTeam.isAttacking() && simpleTeam.IsShootOpen(transform.position)) {
			team.SendMessage(MSG_PASS_TO_ME, transform.gameObject);
		}
	}
	
	public void ShootPassOrContinueDribble() {
		//see if we can make a goal
		if(simpleTeam.IsShootOpen(transform.position)) {
			Shoot();
		} else {
			//otherwise pass to another player if we are threatened and we've had the ball long enough
			float timeElapsed = Time.time - arriveSpotTime;
			if(timeElapsed > Constants.MIN_DRIBBLE_TIME && simpleTeam.IsThreatened(transform.position)) {
				Debug.Log("Threatened - finding a pass player");
				//see if another player is free for a pass
				GameObject passPlayer = simpleTeam.FindPassPlayer(gameObject);
				if(passPlayer) {
					PassTo(passPlayer);
				} else {
					Shoot();
				}
				return;
			}
			dribbleRegion = simpleTeam.SimpleSpotCalculator.BestDribblePosition(simpleTeam.isTeam1);
		}
	}
	
	private void Shoot() {
		//move the ball to the goal
		Debug.Log("shoot!");
		//aim for the mid center of the goal
		Vector3 shot = new Vector3(simpleTeam.Goal.transform.position.x, simpleTeam.Goal.transform.position.y / 2, simpleTeam.Goal.transform.position.z);
		simpleSoccerBall.GoTo(AddNoiseToKick(shot));
		team.SendMessage(MSG_SHOOT, transform.gameObject);
		ChangeState(SimpleState.WAIT.Instance);
	}
	
	public void PassTo(GameObject passPlayer) {
		//kick ball to the other player
		Debug.Log("pass " + gameObject.ToString() + " to " + passPlayer.ToString());
		animation.Play("pass");
		//move player out of the way of the pass
		soccerBall.transform.LookAt(passPlayer.transform.position);
		transform.position = soccerBall.transform.position - soccerBall.transform.forward / 1.5f;
		//tell the other guy to wait for the pass
		SimplePlayerField simplePlayerField = passPlayer.GetComponent<SimplePlayerField>();
		simplePlayerField.ReceivePass(passPlayer.transform.position);
		//move the ball
		simpleSoccerBall.GoTo(AddNoiseToKick(passPlayer.transform.position));
		ChangeState(SimpleState.WAIT.Instance);
	}
	
	private Vector3 AddNoiseToKick(Vector3 targetPosition) {
		float rand = (1 - Constants.KICK_ACCURACY) * Random.Range(-1f, 1f);
		Vector3 displacement = new Vector3(1 - Mathf.Sin(rand), 1, 1 - Mathf.Cos(Mathf.PI / 2 - rand));
		return new Vector3(targetPosition.x * displacement.x, targetPosition.y, targetPosition.z * displacement.z);
	}
	
	public float GoTo(Vector3 position, float speed, string animationType) {
		animation.Play(animationType);	
		transform.LookAt(new Vector3(position.x, transform.position.y, position.z));
		transform.position += transform.forward * Time.deltaTime * speed;
		float distance = (transform.position - position).magnitude;
		return distance;
	}
	
	public Vector3 HomePosition() {
		return (simpleTeam.isAttacking()) ? attackHomeRegion : defendHomeRegion;
	}
	
	public bool AtHomePosition () {
		float distance = (transform.position - HomePosition()).magnitude;
		return (distance < Constants.ARRIVE_DISTANCE);
		
	}
	
	//***messages from team***
	void Msg_ReceiveBall () {
		ChangeState(SimpleState.CHASE_BALL.Instance);
	}
	
	void Msg_GoHome () {
		ChangeState(SimpleState.RETURN_TO_HOME_REGION.Instance);
	}
	
	void Msg_PassTo (GameObject player) {
		PassTo(player);
	}
	
	void Msg_Support (Vector3 gotoPosition) {
		supportRegion = gotoPosition;
		ChangeState(SimpleState.SUPPORT_ATTACKER.Instance);
	}

}
