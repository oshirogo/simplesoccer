﻿using UnityEngine;
using System.Collections;

public class SimplePlayerGoal : SimplePlayerBase {

	void Start () {
		init ();

		animation["goalkeeper_clear_right_up"].speed = 1.0f;
		animation["goalkeeper_clear_left_up"].speed = 1.0f;
		animation["goalkeeper_clear_right_down"].speed = 1.0f;
		animation["goalkeeper_clear_left_down"].speed = 1.0f;

		animation.Play("rest");
		
	}
}
