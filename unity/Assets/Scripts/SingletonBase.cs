﻿public class SingletonBase<T> where T : SingletonBase<T>, new() {
	
	private static T instance = new T();
	
	public static T Instance
	{
		get 
		{
			return instance;
		}
	}
}